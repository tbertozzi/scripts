# scripts

This is a repository of miscellaneous scripts and wrappers used mostly for processing/analysis of sequencing data that are not intimately tied to a single project. The scripts are categorised by either the file type they manipulate or general use.