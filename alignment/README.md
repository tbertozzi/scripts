## Scripts for working with sequence alignments

**runMuscle** - Align sequences in one or more fasta files with Muscle  
**catAlignedLoci** - Concatenate sequences in aligned fasta formatted files by name  
**filterAlignedLoci** - Subset many aligned fasta files by sample list

