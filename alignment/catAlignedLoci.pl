#!/usr/bin/perl -w

# This script traverses the sequences in one or more aligned fasta formatted files and  
# concatenates them into a single sequence per sample based on the order of loci from a coordinate file (BED4).
# The script has been written to concatenate consensus sequences from exon capture studies
# and will generate a "candidate partition" file based on the locus boundaries that can
# be used for downstream phylogenomic analyses (e.g. partition finder). Users can specify
# a threshold when running the script to filter out loci that contain too many missing
# sequences (ie ALL Ns). Only entries in the BED file are considered, allowing further
# filtering of output.
#
# Output is a new fasta file including each sample and a boundary file.
#
# NOTE: Each file must have the same number of sequences with identical sequence names
#
# Run without parameters for help
#
# Terry Bertozzi
# September 2018

use 5.22.0; # some structures require this perl version or better
use strict;
use Getopt::Long;
use File::Basename;

my %sample; # hash of arrays
my @locus_order;
my @locus_length;
my $seq_num;

my ($path, $threshold, $bedfile);
my $opt=GetOptions("path=s"=>\$path,
				   "bed=s" =>\$bedfile,
                   "thresh=f"=>\$threshold);

# check the required inputs
unless (defined ($path && $threshold && $bedfile)) {
    usage();
}

unless($threshold>=0 && $threshold<=100){
    print STDERR "Threshold percentage must be between 0 and 100\n";
    usage();
}

$path=$path."/" unless ($path=~/\/$/); #check for trailing /

open (BED, $bedfile) or die ("Can't open file $bedfile $!");

#---- process the BED4 ----
while (my $line = <BED>){
    chomp $line;
    my @line = split(/\t/,$line);
#    push @locus_order, $line[3];    # keep a record of the locus order
    
    # open the locus alignment
    $line[3]=~s/\|/_/;
    my $file = $path.$line[3]."_filtered.afa";
    open (FH, '<', $file) or die ("Can't open file $file $!");
    
    my %alignment; 
    my $missing=0;
    my @seq_lengths;
    
    #--- process the locus alignment ---
    my @aux = undef;
    my ($name, $seq, $qual); 

    while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
        if (exists ($alignment{$name})){ # sanity check for duplicated fasta headers
            print "$file contains duplicates sequence names";
            exit;
        }else{
            $alignment{$name}=$seq;
            $missing++ if !($seq=~/[^N-]+/); #updated to detect alignment gaps and whole string
            push @seq_lengths, length($seq);
        }
    }
    # sanity check for sequence length
    for (my $i=1; $i<=$#seq_lengths; $i++){
        if ($seq_lengths[$i] != $seq_lengths[0]){
            print "Sequences for locus $line[3] are not all the same length\n";
            exit;
        }
    }

    # include the locus if proportion missing is less than threshold
    my $alignment_size = keys %alignment;
    if (($missing/$alignment_size)*100 <= $threshold){   
        foreach my $key (keys %alignment){
            push @{$sample{$key}},$alignment{$key};
        }
            push @locus_order, $line[3];    # keep a record of the locus order
            push @locus_length, $seq_lengths[0]; # keep a record of the locus length
    }
}

# output
my $outfile = $path."concatenated_alignment.fasta";
my $outpart = $path."alignment_partitions.txt";

open (OUT, '>', $outfile) or die ("Can't open $outfile for output $!");
for my $key (sort keys %sample){
    print OUT ">$key\n";
    print OUT join("",@{$sample{$key}})."\n";
}
close OUT;

open (BOUND, '>', $outpart) or die ("Can't open $outpart for output $!");
my $start=0;
my $stop=0;
for (my $i=0; $i<=$#locus_order; $i++){
    $start=$stop+1;
    $stop=$stop+$locus_length[$i];
    print BOUND "$locus_order[$i] = $start-$stop;\n";
}
close BOUND;

sub readfq {
#Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment

    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

sub usage {
        my $progname = basename($0);
        print STDERR "\nExample usage:\n\n";
        print STDERR "$progname --path <string> --bed <string> --thresh <float>\n\n";
        print STDERR "Required:\n";
        print STDERR "      --path <string> = path to multiple aligned fasta files to concatenate\n";
		print STDERR "      --bed <string> = bed file dictating concatenation order\n";
        print STDERR "      --thresh <0-100> = percent missing sequences allowed to include a locus\n\n" ;   
        exit;    
}

exit;
