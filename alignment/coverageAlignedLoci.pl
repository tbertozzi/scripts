#!/usr/bin/perl -w

# This script takes a directory of aligned exons and will output;
#   (1) a table of coverage stats (how much of target) for each sample (including alignment length for each locus and total concatenated alignment)
#   (2) a summary table of numbers of loci above 10, 20, 30..90% thresholds for each sample
#
# Usage Notes:
#	- All sequences in an alignment need to be of equal length
#	- All alignments need to have the same sample names, regardless of whether they have data or not
#   - Output files will be in the directory this script is called from
#
# Usage: perl coverageAlignedLoci.pl <relative path to aligned exons>
#
# Terry Bertozzi
# September 2024

use strict;
use Cwd;
use POSIX qw(strftime);
use List::Util qw/sum/;
use File::Find;

my @loci;
my @alignlen;
my %samples; # Hash of arrays

my $basedir = cwd();
my $alignedir= "$basedir/$ARGV[0]";
my $timestamp = strftime "%Y-%m-%dT%H%M%S",localtime;
my $sflag;


# process the alignments
opendir(DH, $alignedir) or die "Can't open directory $alignedir. Please ensure you entered a relative path to the aligned exons.\n";

# for each alignment
while (defined(my $file = readdir(DH))) {
    next if ($file =~ m/^\./); #don't process the . and .. directories
    my %alignment;
    my @locus = split(/_/,$file);
    push @loci, join("_",$locus[0],$locus[1]);
    open (FH, "$alignedir/$file") or die ("Can't open $file for processing\n");
    my $seqlen;
    
    my @aux = undef;
	my ($name, $seq, $qual);
	while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
        $seqlen = length($seq);
#        my @header = split(/,/,$name);
        my ($base_count, $iupac_count, $ncount, $gaps) = base_scan($seq);
        my $coverage = sprintf "%.0f", (($base_count + $iupac_count) / $seqlen)*100; # round base coverage to nearest percent

		# get a list of the sample names from the first file processed
		if(!defined $sflag){
			unless(exists($samples{$name})){
			    $samples{$name}=[];
			}else{
				die ("$file contains a duplicate sample name\n");
			}
		} 

        unless(exists($alignment{$name})){
            $alignment{$name} = $coverage;
        }
	}
    $sflag=1; #only run for first file
       
    push @alignlen, $seqlen;
    # update the sample hash with this alignment
    foreach my $sample (keys %samples){
        if(exists($alignment{$sample})){
            push @{ $samples{$sample} }, $alignment{$sample};
        }else{
            push @{ $samples{$sample} }, "0";
        }
    }            
}

close FH;
closedir DH; 
 
# output the coverage data and summary thresholds as csv
open (OUT, ">", "$basedir/coverage_$timestamp.csv") or die ("Can't open coverage_$timestamp.csv for output\n");
open (OUT2, ">", "$basedir/thresholds_$timestamp.csv") or die ("Can't open thresholds_$timestamp.csv for output\n");

# print headers
print OUT "Sample,",join(",",@loci),"\n";
print OUT2 "Sample,10%,20%,30%,40%,50%,60%,70%,80%,90%\n";

for my $sample(sort keys %samples){
    my ($thresh10, $thresh20, $thresh30, $thresh40, $thresh50, $thresh60, $thresh70, $thresh80, $thresh90)=(0) x 9;
	print OUT "$sample,", join(",", @{ $samples{$sample} }),"\n";
    foreach my $element (@{ $samples{$sample} }){
        $thresh10 ++ if $element >= 10;
        $thresh20 ++ if $element >= 20;
        $thresh30 ++ if $element >= 30;
        $thresh40 ++ if $element >= 40;
        $thresh50 ++ if $element >= 50;
        $thresh60 ++ if $element >= 60;
        $thresh70 ++ if $element >= 70;
        $thresh80 ++ if $element >= 80;
        $thresh90 ++ if $element >= 90;
    }
    print OUT2 "$sample, $thresh10, $thresh20, $thresh30, $thresh40, $thresh50, $thresh60, $thresh70, $thresh80, $thresh90\n";    
}
print OUT "\n", "locus alignment length,", join(",",@alignlen),"\n";
print OUT "Total alignment length,", sum(@alignlen);
close OUT; 
close OUT2;
 

sub base_scan {
    my ($scanseq) = @_;
    my $base_count=($scanseq=~tr/ACGTacgt//);
    my $iupac_count=($scanseq=~tr/RYKMSWBDHVrykmswbdhv//);
    my $ncount=($scanseq=~tr/Nn//);
    my $gaps=($scanseq=~tr/-//);
    return ($base_count, $iupac_count, $ncount, $gaps);   
}

sub readfq {
    # Heng Li's PERL port of readfq
    # 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
        while (<$fh>) {
            chomp;
            if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
            	$aux->[0] = $_;
            	last;
            }
        }
        if (!defined($aux->[0])) {
            $aux->[1] = 1;
            return;
        }
    }
    $_=~ s/\s+/,/g; #replace the spaces in the header with commas for parsing -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    while (<$fh>) {
        chomp;
        $c = substr($_, 0, 1);
        last if ($c eq '>' || $c eq '@' || $c eq '+');
        $seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
        chomp;
        $qual .= $_;
        if (length($qual) >= length($seq)) {
            $aux->[0] = undef;
            return ($name, $seq, $qual);
        }
    }
    $aux->[1] = 1;
    return ($name, $seq);
}
