#!/bin/bash

# This script will search for all fasta files (.fasta | .fa) in a directory and filter (keep) the sequences
# in each based on a provided list of deflines (ie the fasta headers). Optionally, it will also count and 
# ouput the number of sequences retained in each file.

# Requirements: seqtk must be installed and in path

# Terry Bertozzi
# April 2024

PROGNAME=$(basename $0)

function usage
{
	# Usage information
	# Accepts no args
	# 
	echo
	echo "$PROGNAME -i <path> -o <path> -l <file> [-c|h]"
	echo
	echo "Required:"
	echo "-i	Directory path containing fasta files"
	echo "-o	Directory path for the filterd output files"
	echo "-l	Defline list to filter by (one per line)"
	echo
	echo "Optional:"
	echo "-c	Summary file of sequence counts for each fasta file"
	echo "-h	Print this help"
	echo
	echo "Note: seqtk must be iinstalled and in the path"
	echo
}


function error_exit
{
    # Exit function due to fatal error
    # Accepts 1 arg:
    # string - descriptive error message

    echo "${PROGNAME}: ${1:-"Unknown error"}" 1>&2
    usage
    exit 1
}


#----- I/O -----

# set our defaults for the options
INPATH=0
OUTPATH=0
LIST=0
COUNT=0

# parse the options
while getopts 'i:o:l:c:h' opt ; do
    case $opt in
        i) INPATH=$OPTARG;;
		o) OUTPATH=$OPTARG ;;
		l) LIST=$OPTARG ;;
		c) COUNT=1 ;;
		h) usage
		   exit ;;
		*) usage
		   exit ;;
    esac
done

# deal with no options
if [ $OPTIND -eq 1 ]; then
	usage
	exit
fi

# skip over the processed options
shift $((OPTIND-1)) 

#sanity checks

# check input path exists
if [ ! -d $INPATH ]; then
	error_exit "$LINENO: Input directory path does not exist"
fi

# make sure there are some fasta files in the input directory	
files=(`find $INPATH -maxdepth 1 -name "*.fasta" -o -name "*.fa"`)
if [ ! ${#files[@]} -gt 0 ]; then
	error_exit "$LINENO: Input directory does not appear to contain fasta files"
fi

# check that the list file exists and is not empty
if [ ! -s $LIST ]; then
	error_exit "$LINENO: The sample list does not exist or is empty"
fi

# make sure we have an outpath
if [ $OUTPATH == 0 ]; then
		error_exit "$LINENO: You need to provide an output path"
fi

mkdir -p $OUTPATH

#----- main -----

# filter the fasta files
for f in "${files[@]}"
do
	FILE=${f##*/}
	seqtk subseq $f $LIST > "$OUTPATH/$FILE" || error_exit "$LINENO: Error filtering file $f"
done

# optionally count the sequences in each output file and write to file
if [ $COUNT == 1 ]; then
	declare -A counts
	outfiles=(`find $OUTPATH -maxdepth 1 -name "*.fasta" -o -name "*.fa"`)
	
	for f in "${outfiles[@]}"
	do
		file=${f##*/}
		fstem=${file%.fa*}
		numseq=$(grep -c ">" $f)
		counts[$fstem]=$numseq
	done
fi

if [ ${#counts[@]} -gt 0 ]; then
	for key in "${!counts[@]}"; do
		printf "%s:%s\n" "$key" "${counts[$key]}" >> $OUTPATH"/counts.txt"
	done
fi

