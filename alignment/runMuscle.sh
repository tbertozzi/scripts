#!/bin/bash


# This script takes a path to multiple fasta formatted sequence files in a directory tree and aligns the sequences
# in each file with muscle. All sequences ahould be orientated in the same coding strand.
#
# Output is either a multiple sequence alignment and optionally a consensus sequence if the -c option is selected.
#
# Dependencies:	muscle must be installed and in path 
# 				      EMBOSS package must be installed and in path to create consensus sequnces
#
# Usage: runMuscle [-c] /path/to/top/level/dir
#
# Terry Bertozzi
# 16.viii.17


PROGNAME=$(basename $0)

function error_exit
{
    # Exit function due to fatal error
    # Accepts 1 arg:
    # string - descriptive error message

    echo "${PROGNAME}: ${1:-"Unknown error"}" 1>&2
    exit 1
}


#----- I/O -----

# set our defaults for the options


# parse the options
while getopts 'c' opt ; do
    case $opt in
        c) consensus=1;;
    esac
done

# skip over the processed options
shift $((OPTIND-1)) 

# process the files
find $1 -type f -print0 | while IFS= read -r -d '' file
do
	path=${file%/*}
	filename=${file##*/}
	sample=${path##*/}

	mkdir -p $path"_MSA"
	
	muscle -in $file -out $path"_MSA/"$filename".afa"

	if [ $consensus = 1 ];then
		mkdir -p $1"/consensus"
		outfile=$sample"_"$filename
		# compute msa consensus with EMBOSS cons program
		cons -sequence $file -outseq $1"/consensus/"$outfile".fa" -name $outfile
	fi
done













