## Scripts for working with exon capture data

**filterMergedLoci** - Filter sequences likely to be the result of merging multiple loci  
**groupTargets** - Creates files of the same target sequence from different samples ready for alignment
