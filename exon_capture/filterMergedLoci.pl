#!/usr/bin/perl -w

# This script traverses the sequences in one or more aligned fasta formatted file(s) and  
# attempts to determine if any of the sequences are likely to be result of merging more than  
# one locus based on the number of heterozygous sites/length of sequence and a user provided 
# threshold percentage. Sequences containing an excess of heterozygous sites will be replaced 
# with a string of N the same length of the original sequence.
#
# Output is to new directory (old_filtered) if path and new file name (oldname_filtered) if file. 
#
# currently only considers 2-base IUPAC codes
#
# 


use strict;
use Getopt::Long;
use File::Basename;

my @files;
my $outdir;


my ($infile, $threshold);
my $opt=GetOptions("file=s"=>\$infile,
                   "thresh=f"=>\$threshold);

if (!($opt && $infile && $threshold)) {#check for the required inputs
    usage();
}

# Determine if we have a single file or path
if (-d $infile){
    opendir(DH, $infile) or die "Can't open directory $infile";
    while (defined(my $file = readdir(DH))) {
        next if ($file =~ m/^\./); #don't process the . and .. directories
        push @files, "$infile/$file";
        $outdir = $infile."_filtered";
        unless(-e $outdir || mkdir $outdir){
            die "Unable to create $outdir\n";
        }
    }
    close DH;
}elsif (-f $infile){
    push @files, $infile;
    my ($fname,$fdir,$fext)= fileparse($infile,'\..*');
    $outdir = $fdir;
}else{
    print "The --file argument does not appear to be a valid file or directory\n\n";
    usage();
}

# Process the file(s)
while (@files){
    my $file=shift @files;
    open (FH, $file) or die ("Can't open file $file $!");

    my ($fname,$fdir,$fext)= fileparse($file,'\..*');
    my @aux = undef;
    my ($name, $seq, $qual);
    
    my %seqhash;
    while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
        #chomp $seq;
        my $seqlen = length($seq);
        my $n_count = ($seq=~tr/N/N/);
        if ($n_count == $seqlen){
            $seqhash{$name} = $seq;
            next;
        }
        my $iupac_count = ($seq=~tr/RMYKSW/RMYKSW/);
        my $gap_count = ($seq=~tr/-/-/);
        if (($iupac_count/($seqlen-$n_count-$gap_count))*100 > $threshold){
            $seqhash{$name} = "N" x $seqlen;
        }else{
            $seqhash{$name} = $seq;
        }
    }
    
    my $outfile = $outdir."/".$fname."_filtered".$fext;
    open (OUT,">", "$outfile") or die "Can't open $outfile for output\n";

    foreach my $key (sort keys %seqhash){
        print OUT ">$key\n$seqhash{$key}\n";
    }    
    close OUT;
    
}

sub readfq {
#Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment

    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

sub usage {
        my $progname = basename($0);
        print STDERR "\nExample usage:\n\n";
        print STDERR "$progname --file <file|path> --thresh <float>\n\n";
        print STDERR "Required:\n";
        print STDERR "      --file <file|path> = fasta file or path to multiple fasta files\n";
        print STDERR "      --thresh <float> = threshold percentage for considering merged loci\n" ;   
        exit;    
}

exit
