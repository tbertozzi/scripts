#!/usr/bin/perl -w

# The script takes fasta files created with applyVariants.pl and groups the same target sequence
# from different samples into a separate file for each target ready for alignment. Note that targets
# may not have the same sample order (aligning them will mess up the order anyway).
#
#   Usage: perl groupTargets.pl <path to fasta files>
#
#Terry Bertozzi 22.iii.18
#

use strict;
use File::Basename;

my %targets;

opendir(DH, $ARGV[0]) or die "Can't open directory $ARGV[0]";
my $outdir = $ARGV[0]."/targets";
mkdir $outdir unless -d $outdir;

# for each file
while (defined(my $file = readdir(DH))) {
    next unless ($file=~/\.fasta$/i); # only process fasta
    if (!(-z $file)) {
        open (FH, $file) or die ("Can't open file '$file' : $!\n");
        my ($fname, $dir, $ext) = fileparse($file,'\..*');
        
        # parse the fasta
        my @aux = undef;
        my ($name, $seq, $qual);
        while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
            my $outfile;
            chomp $seq;

#            if(exists($targets{$name})){
            $targets{$name}{$fname}=$seq;
        }
        close FH;
    }
}

closedir (DH);

foreach my $target (keys %targets){
    my $outfile = $target;
    $outfile=~s/\|/_/;    #get rid of pipe symbols
    open (OUT, ">>$outdir/$outfile") or die ("Can't open file '$outfile': $!\n");
    foreach my $sample (keys %{$targets{$target}}){
        print OUT ">$sample\n";
        print OUT "$targets{$target}{$sample}\n";
    }
}

sub readfq {
#Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment

    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}
