## Scripts for working with fasta files
**catFasta** - Concatenate sequences in a fasta file  
**chunkFasta** - Break a fasta file into muliple fasta files by sequence number  
**getFastaDefLine** - Get the definition lines from a fasta file  
**modifyFastaDefLine** - Prepend / append text strings to fasta defintion lines  
**sortFasta** - Sort the sequences in a fasta file  
**stripGaps** - Strip alignment gaps from fasta formatted sequences

