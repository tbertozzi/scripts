#!/usr/bin/perl -w
#
# Take a file of FASTA formatted sequences and concatenate them, separating the sequences with a user defined number of Ns
# (default = no Ns) and controlling whether to begin or terminate with Ns (default = no beginning or terminating Ns) and the
# creation of BED4 file (default = no BED file).
#
# Terry Bertozzi
# November 2016

use strict;
use warnings;

use File::Basename;
use Getopt::Std;

my $sep;
my $cat_string="";
my $index=0;
my @bed;
my %opts;


getopts('n:f:btB', \%opts);

#check for required inputs
unless ($opts{f}){
    print_usage();
}

my $infile = $opts{f};

if ($opts{n}){
    $sep = 'N' x $opts{n};
}else{
    $sep = "";
}

unless (open (FH, $infile)) {
    die ("Can't open infile $infile\n");
}
	
my ($fname, $dir, $ext) = fileparse($infile,'\..*');
my $outfile = $fname."_concat".$ext;
my $bedfile = $fname."_concat.bed";

unless (open (OUT, ">$outfile")) {
    die ("Can't open $outfile for output\n");
}
 
my @aux = undef;
my ($name, $seq, $qual);

print OUT ">concat_seq\n";
$cat_string = $sep if ($opts{b});
$index = $opts{n} if ($opts{b});

while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
    chomp $seq;
    my $len = length($seq);
    push @bed, {start => $index, stop => $index+$len, name =>$name};
    $index = $index + $len + $opts{n};
    
    $cat_string = $cat_string.$seq.$sep;
}

unless ($opts{t}) {
    $cat_string = substr($cat_string, 0, -$opts{n});
}

print OUT $cat_string;

close FH;
close OUT;

if ($opts{B}){
    unless (open (BED, ">$bedfile")) {
        die ("Can't open $bedfile for output\n");
    }
    
    for my $href (@bed){
        print BED "concat_seq\t$href->{start}\t$href->{stop}\t$href->{name}\n";
    }
    
    close BED;
}

sub print_usage {
        my $progname = basename($0);
        print STDERR "\nExample usage:\n\n";
        print STDERR "$progname -f <fasta filename> [-n <integer>] [-btB]\n\n";
        print STDERR "Options:\n";
        print STDERR "      -n = Number of Ns to separate sequences (default 0)\n";
 				print STDERR "      -b = Add Ns to the beginning\n";
        print STDERR "      -t = Add Ns to the end\n";
        print STDERR "      -B = Toggles creation of BED file\n\n";
        exit;    
}

#Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment

sub readfq {
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

exit;

