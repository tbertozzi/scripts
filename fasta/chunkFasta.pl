#!/usr/bin/perl -w

# This script will chunk a fasta file containing multiple sequences into multiple fasta files each with a
# maximum number of sequences specified by user input

# Terry Bertozzi
# Sept 2016

use strict;
use Getopt::Long;
use File::Basename;

my $infile;
my $max_seq;

my $opt=GetOptions("fasta=s"=>\$infile,
                   "num=i"=>\$max_seq);


if (!($opt && $infile && $max_seq)) { #check for the required inputs
    print STDERR "\nExample usage:\n\n";
    print STDERR "$0 --fasta <filename> --num <max number of seqeunces per chunk> \n\n";
}

unless (open (FH, $infile)) {
    die ("Can't open infile $infile\n");
}

my $n = 1; # first file chunk suffix
my $count = 0; #sequence count

my ($fname, $dir, $ext) = fileparse($infile,'\..*');
my $outfile= sprintf "%s-%05d\.fasta", $fname, $n;
unless (open (OUT, ">$outfile")) {
        die ("Can't open $outfile for ouput\n");
}

my @aux = undef;
my ($name, $seq, $qual);

while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
    if ($count < $max_seq) {
        print OUT ">$name\n$seq\n";
        $count++;
    }else{
        close OUT;
        $count = 1; #reset count
        $n++; #increment file suffix
        
        my $outfile= sprintf "%s-%05d\.fasta", $fname, $n;
        unless (open (OUT, ">$outfile")) {
            die ("Can't open $outfile for ouput\n");
        }
        print OUT ">$name\n$seq\n";
    }
}   
close FH;
close OUT;
 
 

# Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment
sub readfq {
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

exit;
