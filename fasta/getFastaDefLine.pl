#!/usr/bin/perl -w

# Get the definition lines (ie fasta headers) from fasta formatted sequences
#
#Terry Bertozzi 3.vi.15
#Version 1

use strict;
use File::Basename;


unless (open (FH, '<:crlf', $ARGV[0])) { #use PerlIO layer to handle windows files
    die ("Can't open infile $ARGV[0]\n");
}
 
my ($fname, $dir, $ext) = fileparse($ARGV[0],'\..*');
my $outfile = $fname.".defline";
 
unless (open (OUT, ">", $outfile)) {
    die ("Can't open $outfile for output\n");
}

print OUT "defline\n";

my @aux = undef;
my ($name, $seq, $qual);

while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
    $name =~ s/>//;
    print OUT "$name\n";
}

close FH;
close OUT;



# Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment
sub readfq {
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = $_; #keep the whole name -TB (original code my $name = /^.(\S+)/? $1 : '';)
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

exit;

