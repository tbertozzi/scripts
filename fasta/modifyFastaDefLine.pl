#!/usr/bin/perl -w

# Rename fasta formatted deflines by appending / prepending a text string using
# a specified separator (defaults to underscore).
#
# NOTE: Your choice of separator may affect the ability of downstream applications
# to process your headers properly.
# 
#
#Terry Bertozzi 8.ix.14


use strict;
use Getopt::Long;
use File::Basename;

my %temphash=();
my $pre='';  # Text to prepend to sequence name
my $post=''; # Text to append to sequence name
my $infile; # Sequence file to rename
my $outfmt=''; #Control output as fastq (if input was fastq) or fasta (both)
my $sep=''; # Separator between new and existing text

my $opt=GetOptions("pre=s"=>\$pre,
                   "post=s"=>\$post,
                   "sep=s"=>\$sep,
		   "in=s"=>\$infile,
		   "outfmt=s"=>\$outfmt);



if (!($opt && $infile && ($pre || $post))) {#check for the required inputs
    print STDERR "\nExample usage:\n\n";
    print STDERR "$0 --in <filename> --pre <text> and/or --post <text> --sep [text] > <outfile>\n\n";
    exit;
}

# set a default separator if none specified
$sep = "_" if (!$sep);

#TODO:need to check input file type and  error message plus help if try to rename a fasta file and output to fastq


#default to fasta output
unless ($outfmt eq 'fq'){
    $outfmt='fa';
}

unless (open (FH, $infile)) {
    die ("Can't open infile $infile\n");
}
 
my @aux = undef;
my ($name, $seq, $qual);

while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
    substr $name, 0, 1, "";
    $name = $pre.$sep.$name if($pre);
    $name = $name.$sep.$post if($post);
    if ($outfmt eq "fa"){
	print  ">$name\n$seq\n";
    } else {
	print  "$name\n$seq\n+\n$qual\n";
    }
}

close FH;
    

# Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment
sub readfq {
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = $_; #keep the whole name -TB (original code my $name = /^.(\S+)/? $1 : '';)
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

exit;
