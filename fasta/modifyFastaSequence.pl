#!/usr/bin/perl -w

# Modify fasta formatted sequences in various ways.
# 
# Input is a fasta formatted file or directory of fasta files (.fasta|.fa) with any number of sequences
# and one option from each option group. 
#
# Output depends on input"
#	- if a single file is passed, a modified fasta file postfixed with "_modseq" is ouput
#	- if a directory path is passed, each fasta file in that directory will be processed and output
#	  into a subdirectory called "modseq"
#

# Terry Bertozzi 
# September 2024


use strict;
use Getopt::Long;
use File::Basename;

my %temphash=();

my $input; # Sequence file to modify
my $gaptoN=''; # Convert (-) gaps to N
my $exgaptoN=''; # Convert extended gap characters (-, x, X) to N
my $strip=''; #Strip gaps (-)


my $opt=GetOptions('in=s'=>\$input, 
				   'gap2N'=>\$gaptoN,
                   'exgap2N'=>\$exgaptoN,
		           'strip'=>\$strip);


# check for an input
unless ($input){
    print_usage();
} 


# -- gap modifications --
# check a gap modification was selected
if (!($gaptoN || $exgaptoN || $strip)){
	print "\nPlease select a modification option\n";
    print_usage();
}

# check only one gap modification was selected
if (($gaptoN && $exgaptoN) || ($gaptoN && $strip) || ($exgaptoN && $strip) || ($gaptoN && $exgaptoN && $strip)) {
	print "\nOnly one gap modification can be used at a time\n";
	print_usage();
}

# -- main --
if (-f $input){
	my ($fname, $dir, $ext) = fileparse($input,'\..*');
	my $outfile = $fname."_modseq".$ext;
	modify_file($input,$outfile); 

# FUTURE: May want to consider passing file handles instead if multiple operations are needed

}elsif (-d $input){
	# process the directory of files
	opendir(DH, $input) or die "Can't open directory $input";
	$input=~s|/\z||; # remove trailing / if exists  
	my $outpath = $input."/modseq";
	mkdir $outpath unless(-d $outpath);

	# for each file
	while (defined(my $file = readdir(DH))) {
    	next if ($file=~m/^\./); #don't process the . and .. directories
    	next unless ($file=~m/(\.fa|\.fasta)\z/);
    	my $outfile = "$outpath/$file";
    	modify_file("$input/$file","$outpath/$file");
	}
	closedir DH; 
}else{
	print "\nInput in neither a file nor directory path\n";
	print_usage();
}


# -- subs --

sub modify_file{
	#accepts infile and outfile; returns nothing
	my ($in, $out) = @_;
	
	open (IN, $in) or die ("Can't open infile $in for processing\n");
	open (OUT, ">", $out) or die ("Can't open $out for output\n");
	
	my @aux = undef;
	my ($name, $seq, $qual);

	while (($name, $seq, $qual) = readfq(\*IN, \@aux)) {
		$seq=~tr/-/N/ if ($gaptoN);
		$seq=~tr/xX-/N/ if($exgaptoN);
		$seq=~s/-//g if($strip);
		
    	print OUT ">$name\n$seq\n"
	}
	close IN;
	close OUT	
}


sub print_usage {
	my $progname = basename($0);
	print STDERR "\nExample usage: perl $progname --in <fasta file> [gap modifier]\n\n";
	print STDERR "	--in	Fasta fomatted file\n\n";
	print STDERR "	[gap modifiers]\n";        
	print STDERR "	--gap2N 	Convert gaps (-) to N\n";
	print STDERR "	--exgap2Na	Convert extended gaps (-,x,X) to N\n";
	print STDERR "	--strip		Remove gaps (-)\n\n";
	exit;    
}

# Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment
sub readfq {
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = $_; #keep the whole name -TB (original code my $name = /^.(\S+)/? $1 : '';)
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

exit;
