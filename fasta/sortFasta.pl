#!/usr/bin/perl -w

# Rearrange the order of sequences in a fasta file either alphanumerically (default)
# or by predetermined order. The later can be supplied as a list or file. Part of
# sequence name can be provided as long as it is unique. If a full path
# rather than file is supplied, then all *.fasta,*.fa or *.afa files in the directory
# will be processed, non recursively.


use strict;
use Getopt::Long;
use File::Basename;

my @files;

my ($infile, @ids, $idfile);
my $opt=GetOptions("fasta=s"=>\$infile,
                    "list=s"=>\@ids,
                   "listfile=s"=>\$idfile);

if (!($opt && $infile) || (@ids && $idfile) ) {#check for the required inputs
    usage();
}

# allow comma-separated list or multiple occurance of ids
if (@ids){
    @ids=split(/,/,join(',',@ids));
}

# get the ids from file if --listfile option selected
if ($idfile){
    open (FH, $idfile) or die ("Can't open file $idfile\n");

    while (my $line =<FH>) {
        chomp ($line);
        push(@ids,$line);
    }
    close FH;
}

if (-d $infile){
    opendir(DH, $infile) or die "Can't open directory $infile";
    while (defined(my $file = readdir(DH))) {
        next if ($file =~ m/^\./); #don't process the . and .. directories
        push @files, $file;
    }
    close DH;
}elsif (-f $infile){
    push @files, $infile;
}else{
    print "The --fasta argument does not appear to be a valid file or directory\n\n";
    usage();
}


while (@files){
    my $file=shift @files;
    open (FH, $file) or die ("Can't open file $file $!");
  
    my ($fname,$fdir,$fext)= fileparse($file,'\..*');
    my @aux = undef;
    my ($name, $seq, $qual);
    
    my %seqhash;
    while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
        chomp $seq;
        $seqhash{$name} = $seq;
    }
    close FH;

    my $outfile = $fname."_sorted".$fext;
    open (OUT,">", "$outfile") or die "Can't open $outfile for output\n";

    if (@ids){
        #sort by id
        foreach my $id (@ids){
            foreach my $key (keys %seqhash){
                next unless ($key=~/$id/);
                print OUT ">$key\n$seqhash{$key}\n";
            }
        }
    }else{
        #sort alphanumerically
        foreach my $key (sort keys %seqhash){
            print OUT ">$key\n$seqhash{$key}\n"
        }
    }
    close OUT;
    
}




sub readfq {
#Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment

    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

sub usage {
        my $progname = basename($0);
        print STDERR "\nExample usage:\n\n";
        print STDERR "$progname --fasta <file|path> [--list|--listfile]\n\n";
        print STDERR "Required:\n";
        print STDERR "      --fasta <file|path> = fasta file or path to multiple fasta files\n";
        print STDERR "Optional:\n";
        print STDERR "      --list <id1, id2,..> = ordered list to sort by\n\n";
        print STDERR "      --listfile <file> = ordered list in file\n\n";
        exit;    
}

exit;
