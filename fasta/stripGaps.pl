#!/usr/bin/perl

# Strips alignment gaps out of fasta sequences. 
#
# perl stripGaps.pl <filename>
#
# Terry Bertozzi
# December 7, 2017
#
# 


use strict;
use warnings;
use File::Basename;

open (FH, $ARGV[0]) or die ("Can't open infile $ARGV[0]\n");

my ($fname,$fdir,$fext)= fileparse($ARGV[0],'\..*');
my $outfile = $fname."_nogaps".$fext;
open (OUT, ">", $outfile) or die ("Can't open infile $outfile\n");

my @aux = undef;
my ($name, $seq, $qual);

while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
    $seq =~s/-//g;
    print OUT ">$name\n$seq\n"
}    
close FH;
close OUT;


sub readfq {
#Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment

    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

sub print_usage {
        my $progname = basename($0);
        print STDERR "\nExample usage:\n\n";
        print STDERR "$progname --from <path> --to <path> [--intersect]\n\n";
        print STDERR "Required:\n";
        print STDERR "      --to = path to fasta files to be added to\n";
        print STDERR "      --from = path to fasta files to append\n\n";
        print STDERR "Optional:\n";
        print STDERR "      --intersect = keep only files that are in the \"to\" path\n\n";
        exit;    
}

exit;
