## Scripts to help with genbank submission
**buildGenbankDefLine** - Build a genbank style fasta defline  
**buildGenbankFeatureTbl** - Build a feature table for use with the tbl2asn program  
**parseSeqTxtBlock** - Convert an aligned text block (eg nexus format) into a fasta file
  
## Methodology
*A worked example will by provided soon*

The following has been tested using sequence alignment blocks in nexus format

1. Save the sequence alignment block from the nexus file as a text file. The sequence ID must be separated from the sequence by whitespace.
2. Use **parseSeqTxtBlock** to convert the aligned block into fasta format
3. Use [**getFastaDefline**](https://bitbucket.org/tbertozzi/scripts/src/master/fasta) to extract the fasta definition lines
4. Modify the ouput file from step 3. by adding relevant genbank metadata fields. This is easiest to do in a spreadsheet using the genbank metdata tags as column headers. Save in comma delimted format.
5. Use **buildGenbankDefLine** to update/replace the original fasta headers generated by step 2.
6. Use **buildGenbankFeatureTbl** with a sequence template and the fasta file from step 5. to create a feature entry for each sequence in the fasta file. The script will automatically adjust feature lengths and coordinates if necessary after stripping alignment gaps and missing data
7. Use the output files with NCBI's [**table2asn**](https://www.ncbi.nlm.nih.gov/genbank/table2asn/) program to create the genbank submission files which can be emailed to NCBI
