#!/usr/bin/perl -w

# Builds a genbank submission fasta defline and substitute it for the current defline. Will also filter records
# based on the input. For example, if only one seq is used as input, one will be returned. Default is to add the new
# defline to the existing, use the -r flag to totally replace the existing defline with the new one.
#
# requires a comma delimited file in the following format:
# current defline, new defline 1, new defline 2
#
# usage perl buldGenbankDefLine.pl [-r] <csv file> <fasta file>

use strict;
use File::Basename;
use Getopt::Std;


my %deflines;
my %options=();
my @header;
my $index_start=1;

getopts("r",\%options);

unless (open (FH, '<:crlf', $ARGV[0])) { #use PerlIO layer to handle windows files
    die ("Can't open infile $ARGV[0]\n");
}
 
my ($fname, $dir, $ext) = fileparse($ARGV[0],'\..*');
my $outfile = $fname.".defline.fasta";
 
unless (open (OUT, ">", $outfile)) {
    die ("Can't open $outfile for output\n");
}

if($options{r}){
    $index_start=2;
}

# parse the defline file
while (my $line = <FH>) {
    $line =~ s/\R//g; #remove all linefeeds and carrage returns regardless of OS
    if($line =~ /^defline/){
        @header = split(/,/, $line);
        next;
    }

    my @cols = split(/,/, $line);
    my $new_defline;
    
    if($options{r}){
        $new_defline= $cols[1];
    }else{
        $new_defline= $cols[0];
    }
    
    #build the new defline
    for(my $index=$index_start; $index <= $#header; $index++){
        $new_defline = $new_defline." [".$header[$index]."=".$cols[$index]."]";
        
    }
    
    $deflines{$cols[0]}= $new_defline;

}

close FH;

unless (open (FH, '<:crlf', $ARGV[1])) { #use PerlIO layer to handle windows files
    die ("Can't open infile $ARGV[1]\n");
}

my @aux = undef;
my ($name, $seq, $qual);

while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
    $name =~ s/>//;
    if (exists($deflines{$name})) {
        print OUT ">$deflines{$name}\n$seq\n";
    }
}
    
close FH;
close OUT;


# Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment
sub readfq {
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = $_; #keep the whole name -TB (original code my $name = /^.(\S+)/? $1 : '';)
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

exit;
