#!/usr/bin/perl -w

# Builds a feaure table for use with the tbl2asn program. Input is a template file for the sequence fragment
# and the aligned fasta sequence file. The script will create a feature entry for each of the sequences in the
# fasta file and adjust feature lengths and start codons if necessary after stripping gaps and missing data.
#
# usage perl buildGenbankFeatureTbl.pl <template> <fasta file>

use strict;
use File::Basename;


my %sequences;
my @AoH;

# file IO
unless (open (FH, '<:crlf', $ARGV[0])) { #use PerlIO layer to handle windows files
    die ("Can't open infile $ARGV[0]\n");
}

unless (open (IN, '<:crlf', $ARGV[1])) { #use PerlIO layer to handle windows files
    die ("Can't open infile $ARGV[1]\n");
}

my ($fname, $dir, $ext) = fileparse($ARGV[1],'\..*');
my $out_tbl = $fname.".tbl";
my $out_fasta = $fname.".fsa";

unless (open (OUT, ">", $out_tbl)) {
    die ("Can't open $out_tbl for output\n");
}

unless (open (FASTA, ">", $out_fasta)) {
    die ("Can't open $out_fasta for output\n");
}


# parse the template file
while (my $line = <FH>) {
    $line =~ s/\R//g; #remove all linefeeds and carrage returns regardless of OS
    my @cols = split(/\t/,$line);
    if ($cols[0] =~ /^\S+/) {
        push @AoH, {start => $cols[0], stop => $cols[1], feature => $cols[2]};
    }else{
        my $last_index = $#AoH;
        $AoH[$last_index]{tags}{$cols[3]} = $cols[4];
        
    }
}

close FH;


# process each segment of sequence based on the template coordinates
my @aux = undef;
my ($name, $seq, $qual);

while (($name, $seq, $qual) = readfq(\*IN, \@aux)) {
 #   $name =~ s/>//;
    my ($stop, $start);
    my $new_seq;
    my $len_adj=0;
    my $index=0; # a check to make sure the whole sequence is processed (to deal with spacers)
    
    #my ($short_name) = $name=~ /^.(\S+)/;
    $name=~s/^>//;
    print OUT ">Feature $name\n";
    
    for my $href(@AoH){
        # get the template coordinates
        $start = $href->{start};
        $stop = $href ->{stop};
        my ($start_flag) = $start=~s/^<//;
        my ($stop_flag) = $stop=~s/^>//;
        my $codon_start;    
        #print "Index: $index\n"; #########debug
	
		# process non annotated regions (nar)
		if (((($start + $len_adj) - $index) != 1) && ($href->{feature} ne "CDS")){ #is a nar present?
	    	my $nar_len = ($start-1+$len_adj)-$index;
	    	#print "spacer len: $nar_len\n"; #####debug
	    	my $nar = substr($seq, $index-$len_adj, $nar_len);
	    	#print "$nar\n"; ######debug
	    	$nar =~ s/\?|-//g; #remove missing characters and alignment gaps
	    	$new_seq .= $nar; #concatenate the spacer sequence
	    	$len_adj += length($nar)-$nar_len;
	    	$index += length($nar);
	   	 my @seq_id = split(/ /, $name,2);
	    	print "Warning: $seq_id[0] contains a non annotated region of ".length($nar)." bases\n" unless (length($nar) == 0);
		}
	
		
        # process the sequence segment
        my $seg_length= ($stop-$start)+1;
		#print "Raw length: $seg_length\n"; ######debug
        my $segment = substr($seq, $start-1,$seg_length);
        
        if ($href->{feature} eq "CDS"){ #are we processing a coding segment?
            $codon_start = $href->{tags}{codon_start}; #get translation frame
            if ($segment=~/^(-|\?)+/) { #do we need to adjust the translation frame?
                if (length($&) % 3 == 1){
                    if ($codon_start==1) {
                        $codon_start+=2;
                    }else{
                        $codon_start-=1;
                    }
                    
                }elsif (length($&) % 3 == 2){
                    if ($codon_start==3) {
                        $codon_start-=2;
                    }else{
                        $codon_start+=1;
                    }
                }
            }
        }
          
        $segment =~ s/\?|-//g; #remove missing characters and alignment gaps
		#print "Length (no gaps) -".length($segment)."\n"; #########debug
        $new_seq .= $segment unless ($href->{feature} eq "gene"); #concatenate the processed segment
		#print "Start: $start, Stop: $stop\n"; ########debug
	
        $start=$start+$len_adj unless ($start==1);
        $len_adj += length($segment)-$seg_length unless ($href->{feature} eq "CDS");
        $stop = $stop + $len_adj;      
        #print "adj start: $start, adj stop: $stop\n"; ########debug
		#print "adj: $len_adj\n\n"; #########debug
		$index += length($segment) unless ($href->{feature} eq "CDS");
	
        # write out the new feature line
		unless (length($segment) == 0){
	    	$start = "<".$start if ($start_flag);
	   		$stop = ">".$stop if ($stop_flag);
	    	print OUT "$start\t$stop\t$href->{feature}\t\t\n";
        
	
	    	foreach my $key (keys %{$href->{tags}}){
				if ($key eq "codon_start") {
		    		print OUT "\t\t\t$key\t$codon_start\n";
				}else{
		    		print OUT "\t\t\t$key\t$href->{tags}{$key}\n";
				}
	    	}
		}
	
    }
	print OUT "\n";
    print FASTA ">$name\n$new_seq\n"; #write processed sequence

}

close IN;
close OUT;
close FASTA;


# Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment
sub readfq {
    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = $_; #keep the whole name -TB (original code my $name = /^.(\S+)/? $1 : '';)
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}

exit;
