#!/usr/bin/perl -w

# Convert a text block to fasta format
# Sequences must be formatted as <idstring> <whitespace> <sequence>. idstring must have no whitespace or be a quoted string eg "this string". 
#
#Terry Bertozzi 3.vi.15
#Version



use strict;
use File::Basename;


unless (open (FH, '<:crlf', $ARGV[0])) { #use PerlIO layer to handle windows files
    die ("Can't open infile $ARGV[0]\n");
}

my ($fname, $dir, $ext) = fileparse($ARGV[0],'\..*');
my $outfile = $fname.".fasta";
 
unless (open (OUT, ">", $outfile)) {
    die ("Can't open $outfile for output\n");
}

while (my $line = <FH>) {
    my ($seqid, $seq);
    $line =~ s/\R//g; #remove all linefeeds and carrage returns regardless of OS
    $line =~ s/^\s+|\s+$//g; #remove leading and trailing whitespace

    #process text blocks that have quoted strings
    if ($line =~ /^"/) { #double qutoed
        ($seqid) = $line =~ /("[^"]+")/;
        $seqid =~ s/"//g;
    }elsif ($line =~ /^'/) { #single quoted
        ($seqid) = $line =~ /('[^']+')/;
        $seqid =~ s/'//g;
    }else{
        ($seqid) = $line =~ /(^\S+)/;
    }
    
    ($seq) = $line =~ /(\S+$)/;
  
    print OUT ">$seqid\n$seq\n";  
}

close OUT;
close FH;
