#!/usr/bin/perl -w

# This script takes two lists and compares them. Output is controlled by options detailing
# the type of comparison and named accordingly:
#
#	intersection = filename1_and_filename2.txt
#	1 not 2 = filename1_only.txt
#	2 not 1 = filename2_only.txt
# 
#	Run the script with no parameters to get the usage statement



use strict;
use File::Basename;
use Getopt::Std;

my %opts;
my %list;
my @intersection;
my @notlist1;
my @filenames;

getopts('iab', \%opts);

# sanity checks
unless (%opts){
        print "\nPlease select at least one comparison option\n";
        print_usage();
}

if($#ARGV != 1){
        print "\nPlease enter TWO lists for comparison\n";
        print_usage();
}

# process list 1
unless (open (FH, $ARGV[0])) {
    die ("Can't open file $ARGV[0]\n");
}

while (my $line =<FH>) {
    chomp ($line);
    unless (exists($list{$line})){
	$list{$line}=0
    }
}
close FH;

# process list 2
unless (open (FH1, $ARGV[1])) {
    die ("Can't open file $ARGV[1]\n");
}

while (my $line =<FH1>) {
    chomp ($line);
    if(exists($list{$line})){
	push @intersection, $line if($opts{i});
	delete $list{$line};
    }else{
	push @notlist1, $line if($opts{b});
    }
}
close FH1;

# get filenames for output
foreach my $file (@ARGV){
	my ($fname, $dir, $ext) = fileparse($file,'\..*');
	push @filenames, $fname;
}

# output
if($opts{i}){
	my $fh = open_output("$filenames[0]_and_$filenames[1].txt");
	my $OUT = $$fh;
	print $OUT join("\n", @intersection);
	close $OUT;
}
	
if($opts{b}){
	my $fh = open_output("$filenames[1]_only.txt");
	my $OUT = $$fh;
	print $OUT join("\n", @notlist1);
	close $OUT;
}	
	
if($opts{a}){
	my $fh = open_output("$filenames[0]_only.txt");
	my $OUT = $$fh;	
	foreach my $key (keys %list){
		print $OUT "$key\n";
	}
	close $OUT;
}

sub open_output {
	# Accepts - a filename string
	# Returns  - a file handle reference	
	my $outfile= shift;
	my $fh;
	unless (open $fh, '>', $outfile) {
		die ("Can't open $outfile for output\n");
	}
	return $fh;
}

sub print_usage {
        my $progname = basename($0);
        print STDERR "\nExample usage:\n\n";
        print STDERR "$progname [-i|-a|-b] <list 1> <list 2>]\n\n";
        print STDERR "Options:\n";
        print STDERR "      -i = Intersection of both lists\n";
        print STDERR "      -a = In list 1 NOT list 2\n";
        print STDERR "      -b = In list 2 NOT list 1\n\n";
        exit;    
}
exit;
