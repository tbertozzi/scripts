## Scripts for working with variant files

**applyVariants** - Generate diploid alt ref sequences with IUPAC codes  
**filterVCF** - Filter heterozygote calls based on minimum allele frequency
