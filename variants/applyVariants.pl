#!/usr/bin/perl -w

# Generate the diploid sequence inferred by the mapped reads by applying variants in a vcf file to the reference sequence
# used for mapping. A "per base" coverage file is also required and will be used to mask areas below a minimum coverage
# given by "min". The diploid sequence will contain IUPAC ambiguity codes for heterozygous sites. The script cannot assign
# ambiguity codes for heterozygous indels. If present these will be be flagged but no action taken.
#
#NOTE: The BED and fasta features must be in the same order
#NOTE: The output coordinates in the new fasta are 1-based
#
#Terry Bertozzi 15.viii.13
#

use strict;
use File::Basename;
use Getopt::Long;

my %iupac=('A'=>{'C'=>'M','G'=>'R','T'=>'W'},
           'C'=>{'A'=>'M','G'=>'S','T'=>'Y'},
           'G'=>{'A'=>'R','C'=>'S','T'=>'K'},
           'T'=>{'A'=>'W','C'=>'Y','G'=>'K'}
          );
my %variants;
my %warnings;
my @err_variants;
my @err_genotypes;


# IO
my $bedfile; #Interval file in BED6 format
my $fastafile; #Fasta formated sequences from the reference for the intervals in $bedfile
my $vcf; #Filtered VCF file of SNPs /indels
#my $vcftype; #The program that created the VCF - currently supports GATK and freeBayes
my $min_coverage="10"; #Minimum coverage to mask sequence
my $coverage; #File of per-base read coverage for the intervals in $bedfile
my $help;

my $opt=GetOptions("bed=s"=>\$bedfile,
                   "fasta=s"=>\$fastafile,
                   "vcf=s"=>\$vcf,
                   #"vcftype=s"=>\$vcftype,
                   "cover=s"=>\$coverage,
                   "min=i"=>\$min_coverage,
                   'help'=>\$help); 

if (!($opt && $bedfile && $fastafile && $vcf && $coverage) ||$help) {#check for the required inputs
    print STDERR "\nExample usage:\n\n";
    print STDERR "$0 [Options] --bed <filename> --fasta <filename> --vcf <filename> --cover <filename>\n\n";
    print STDERR "Options:\n";
    print STDERR "      --help = This usage example\n\n";
    print STDERR "Required:\n";
    print STDERR "      --bed <filename> = sorted interval file in BED6 format\n";
    print STDERR "      --fasta <filename> = fasta sequences of the reference for the intervals in the BED6 file\n";
    print STDERR "      --vcf <filename> = filtered VCF file of SNPs /indels\n";
#    print STDERR "      --vcftype <gatk|fb> = type of vcf [gatk | freeBayes]\n";
    print STDERR "      --cover <filename> = per-base read coverage for the intervals in the BED6 file\n";
    print STDERR "      --min = the minimum coverage depth to mask sequence (default 10)\n";    
    exit;
}

my ($fname, $dir, $ext) = fileparse($vcf,'\..*');
my @outfile=split("_",$fname,2);
my $outfile=$outfile[0].".fasta";

open (FH, $fastafile) or die ("Can't open file $fastafile\n");
open (BED, $bedfile) or die ("Can't open file $bedfile\n");
open (VCF, $vcf) or die ("Can't open file $vcf\n");
open (COV, $coverage) or die ("Can't open file $coverage\n");

open (OUT, ">$outfile") or die ("Can't open outfile $outfile\n");
open(ERROR,">","$outfile.errors") or die ("Can't open outfile $outfile.errors\n");


# parse the vcf
while (my $line =<VCF>) {
    next if ($line=~/^#/);
    chomp $line;
    my ($chr,$pos,$id,$ref,$alt,$qual,$filt,$info,$format,$genomic)=split(/\t/,$line);
    next if ($filt ne "PASS"); #skip filtered variants
    my @format=split(/:/,$format); #assume VCF contains genotypes
    my @values=split(/:/,$genomic); #values for the keys in @format
    
    my %temp;
    for (my $i=0; $i <= $#format; $i++){
        $temp{$format[$i]}=$values[$i];
    }

    my $variant=$chr.":".$pos;
    unless (exists($variants{$variant})){
        $variants{$variant}={chr=> $chr,ref=>$ref,alt=>$alt,GT=>$temp{'GT'}};
    }else{
        push @err_variants, "$chr:$pos";
    }
}
close VCF;

# log the parsing errors
if (@err_variants){
    print "More than one variant was assigned to these positions: ";
    print ERROR join(",",@err_variants)."\n\n";
}


# parse the bed, fasta and coverage files simultaneously
my @aux = undef;
my ($name, $seq, $qual);

while (($name, $seq, $qual) = readfq(\*FH, \@aux)) {
    my $seq_adj=0;
    my $cov_adj=0;
    my $bed_line=readline(BED);
    chomp $bed_line; #necessary if 4 field BED used
    my ($chr,$start,$stop,$bedname,$score,$strand)=split(/\t/,$bed_line);
    my $interval_len=$stop-$start;

    # make sure we are comparing the right bed interval and fasta sequence - do the names and sequence lengths match?
    if (($name eq $bedname) && ($interval_len == length($seq))){ 
    	$name="$bedname $chr:".++$start."-$stop";
        for(my $seq_pos=0; $seq_pos<=$interval_len-1; $seq_pos++){
            my $cov_line=readline(COV);
            chomp $cov_line;
            if ($cov_adj>0) { #adjust the coverage pointer if a deletion is processed
                for (my $i=1;$i<=$cov_adj;$i++) {
                    $cov_line=readline(COV);
                }
                $cov_adj=0;
            }

            my ($cov_chr,$cov_start,$cov_end,$cov_name,$cov_pos,$depth)=split(/\t/,$cov_line); # - BEDtools coverage     
            # make sure that we are comparing the correct coverage and sequence position
            my $map_pos=$cov_pos+$cov_start;
            if (($cov_chr eq $chr) && ($map_pos==$start+$seq_pos+$seq_adj)){ 
                if ($depth < $min_coverage){
                    substr($seq,$seq_pos,1,"N");
                }else{
                    # if depth is OK check if there is a variant at this position on the right chromosome
                    my $chr_pos=$chr.":".$map_pos;
                    if (exists($variants{$chr_pos})) {                                          
                        # make sure we have the right base
                        my $ref_len=length($variants{$chr_pos}{'ref'});
                        if ((substr($seq,$seq_pos,$ref_len)) eq $variants{$chr_pos}{'ref'} ) { 
                            # if the variant is homozygous alt
                            if ($variants{$chr_pos}{'GT'} eq '1/1' || $variants{$chr_pos}{'GT'} eq '1|1') {
                                if (length($variants{$chr_pos}{'ref'}) == length($variants{$chr_pos}{'alt'})) { # fixed length homozygous snp
                                    substr($seq,$seq_pos,length($variants{$chr_pos}{'ref'}),$variants{$chr_pos}{'alt'});
                                    
                                }elsif (length($variants{$chr_pos}{'ref'}) > length($variants{$chr_pos}{'alt'})){ # deletion
                                    substr($seq,$seq_pos,length($variants{$chr_pos}{'ref'}),$variants{$chr_pos}{'alt'});
                                    my $adjust=length($variants{$chr_pos}{'ref'})-length($variants{$chr_pos}{'alt'});
                                    if ($adjust % 3 != 0) {
                                        $warnings{'frameshift'}{"$chr:$map_pos"}=$variants{$chr_pos}{'ref'}.",".$variants{$chr_pos}{'alt'}; # report possible frameshift
                                    }
                                    $interval_len= $interval_len - $adjust; # decrease the length of sequence to be searched by deletion length
                                    $cov_adj=$adjust; # adjust the coverage position by the length of the deletion
                                    $seq_adj=$seq_adj + $adjust; # make sure the sequence pointer is in sync

                                }elsif (length($variants{$chr_pos}{'ref'}) < length($variants{$chr_pos}{'alt'})){ # insertion
                                    substr($seq,$seq_pos,length($variants{$chr_pos}{'ref'}),$variants{$chr_pos}{'alt'});
                                    my $adjust=length($variants{$chr_pos}{'alt'})-length($variants{$chr_pos}{'ref'});
                                    if ($adjust % 3 ne 0) {
                                        $warnings{'frameshift'}{"$chr:$map_pos"}=$variants{$chr_pos}{'ref'}.",".$variants{$chr_pos}{'alt'}; # report possible frameshift
                                    }
                                    $interval_len= $interval_len + $adjust; # increase the legth of sequence to be searched by insertion length
                                    $seq_pos=$seq_pos + $adjust; # adjust the sequence position by the length of the insertion
                                    $seq_adj=$seq_adj - $adjust; # make sure the sequence pointer is in sync
                                }
                            # if the variant is heterozygous ref
                            }elsif ($variants{$chr_pos}{'GT'} eq '0/1' || $variants{$chr_pos}{'GT'} eq '0|1' || $variants{$chr_pos}{'GT'} eq '1/0' || $variants{$chr_pos}{'GT'} eq '1|0') {
                                if (length($variants{$chr_pos}{'ref'}) == length($variants{$chr_pos}{'alt'})) { # fixed length heterozygous ref snp
                                    my $iupac_str=get_iupac($variants{$chr_pos}{'ref'},$variants{$chr_pos}{'alt'},"$chr:$map_pos");
                                    substr($seq,$seq_pos,length($variants{$chr_pos}{'ref'}),$iupac_str);
                                }else {    
                                     $warnings{'het_indel'}{"$chr:$map_pos"}=$variants{$chr_pos}{'ref'}.",".$variants{$chr_pos}{'alt'};
                                }
                            # if the variant is heterozygous alt
                            }elsif ($variants{$chr_pos}{'GT'} eq '1/2' || $variants{$chr_pos}{'GT'} eq '1|2') { # fixed length heterozygous alt snp
                                my ($alt1, $alt2)=split(/,/,$variants{$chr_pos}{'alt'});
                                if (length($alt1)==length($alt2)) {
                                    my $iupac_str=get_iupac($alt1,$alt2,"$chr:$map_pos");
                                    substr($seq,$seq_pos,length($variants{$chr_pos}{'ref'}),$iupac_str);
                                }else{
                                    $warnings{'het_indel'}{"$chr:$map_pos"}=$variants{$chr_pos}{'ref'}.",".$variants{$chr_pos}{'alt'};

                                }
                            }    
                        }else{
                            print ERROR "Expected base ".$variants{$chr_pos}{'ref'}." not found at position $seq_pos ($cov_pos) in $name\n";
														exit;
                        }
                    }
                }
            } else {
                print ERROR "Sequencing and coverage positions $cov_chr:$cov_pos don't match for BED line $chr:$start:$bedname\n";
								exit;
            }
        }
        print OUT ">$name\n$seq\n";
    } else {
    	print ERROR "Sequence names or lengths don't match for sequence $name and BED $bedname\n";
			exit;
    }
}

close BED;
close FH;
close OUT;

# print out the multilevel hash so that the same warning types are together
if (%warnings){
    unless (open(WARN,">","$outfile.warnings")){
        die ("Can't open outfile $outfile.warnings\n");
    }
    print WARN "Please check the following potential problems:\n";
    foreach my $warning (sort keys %warnings){
        print WARN "$warning:\n";
        foreach my $warn (sort keys %{$warnings{$warning}}){
            print WARN "\t$warn ".$warnings{$warning}{$warn};
            print WARN "\n";
        }
    }
close WARN;
}


# this subroutine takes two heterozygous strings and chr:pos string and returns
# the string as IUPAC amiguity codes
sub get_iupac { 
    my ($str1, $str2, $pos)= @_;
    my @iupac;
    my @str1=split(//,$str1);
    my @str2=split(//,$str2);
    
    while (@str1) {
        my $base1=shift @str1;
        my $base2=shift @str2;
        if (exists($iupac{$base1}) && exists($iupac{$base1}{$base2})){ # check for spurious bases
            push @iupac, $iupac{$base1}{$base2};
        }else{
            $warnings{'bad_base'}{$pos}="$str1,$str2";
            push @iupac, $base1; #leave the ref allele unchanged
        }
    }
    return join("",@iupac);
}


sub readfq {
#Heng Li's PERL port of readfq
# 28.iv.14 - if (defined @array) syntax is deprecated, changes made below, original kept as #comment

    my ($fh, $aux) = @_;
	@$aux = [undef, 0] if (!(@$aux)); #@$aux = [undef, 0] if (!defined(@$aux));
    return if ($aux->[1]);
    if (!defined($aux->[0])) {
	while (<$fh>) {
	    chomp;
	    if (substr($_, 0, 1) eq '>' || substr($_, 0, 1) eq '@') {
		$aux->[0] = $_;
		last;
	    }
	}
	if (!defined($aux->[0])) {
	    $aux->[1] = 1;
	    return;
	}
    }
    #$_=~ s/\s+/_/g; #replace the spaces in the header with underscores -TB
    my $name = /^.(\S+)/? $1 : '';
    my $seq = '';
    my $c;
    $aux->[0] = undef;
    while (<$fh>) {
	chomp;
	$c = substr($_, 0, 1);
	last if ($c eq '>' || $c eq '@' || $c eq '+');
	$seq .= $_;
    }
    $aux->[0] = $_;
    $aux->[1] = 1 if (!defined($aux->[0]));
    return ($name, $seq) if ($c ne '+');
    my $qual = '';
    while (<$fh>) {
	chomp;
	$qual .= $_;
	if (length($qual) >= length($seq)) {
	    $aux->[0] = undef;
	    return ($name, $seq, $qual);
	}
    }
    $aux->[1] = 1;
    return ($name, $seq);
}
