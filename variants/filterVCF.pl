#!/usr/bin/perl -w

# This script allows filtering of heterozygote calls in VCF output files based on a user supplied
# minimum minor allele frequency. Heterozygotes with a minimum alternate allele frequency below the
# threshold with have the filter "maf" applied (retaining the site as homozygous ref). Heterozygotes
# with a minimum reference allele frequency below the threshold will have the GT field changed to 1/1 
# (homozygous alt). Will honour any currently applied filters for hets except "PASS".
#
# The following changes will be made to the meta-information;
#   - FILTER with ID=maf will be added
#   - INFO with ID=HET2A will be added to denote;
#       - 0/1 genotypes changed to 1/1
#       - 1/2 genotypes changed to either allele ***NOT IMPLEMENTED NEED EXAMPLE*** 

# currently for use with freebayes generated VCF
#
# filterVCF.pl --vcf <vcf file> [--freq]
#
# Terry Bertozzi
# October 2017

use strict;
use File::Basename;
use Getopt::Long;

my $variant=();

# IO
my ($vcf, $maf, $help);

my $opt=GetOptions("vcf=s"=>\$vcf,     #check for vcf header
                   "freq=f"=>\$maf,
                   'help'=>\$help);

if (!($opt && $vcf) || $help) { #check for the required inputs

}

# check $min_freq is a proportion
unless ($maf >0 && $maf<1) {
    print STDERR "\nAllele frequency must be between 0 and 1\n";
    exit;
}

# open the vcf file
unless (open (FH, $vcf)){
    die ("Can't open file $vcf\n");
}

my ($fname, $dir, $ext) = fileparse( $vcf,'\..*');
my $outfile= $fname."_mod_hets".$ext;

# open file for output
unless (open (OUT, ">$outfile")) {
        die ("Can't open outfile $outfile\n");
    }

# parse the vcf     
while (my $line =<FH>){
    # TODO - need to parse the header information properly to add filter line,check is VCF and check source
    if(($line=~/^#/) || ($line=~/^\s*$/)){
        print OUT $line; #ignore header and blank lines
        next;
        #parse_header(\*OUT,$line);
    }
    chomp ($line);
    my ($chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,$genomic)=split("\t",$line); #parse the line
    my ($GT,$DP,$RO,$QR,$AO,$QA,$GL)= split(":",$genomic);

    # process the hets    
    if ($GT eq '0/1') {
        unless (($filter eq ".") || ($filter eq "PASS")){
            print OUT "$line\n";
        }else{
            if (($AO/$RO) < 1 && ($AO/$RO) < $maf){
                $filter = "maf";
                apply_filter(\*OUT,$chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,$genomic);
            }elsif (($AO/$RO) > 1 && ($RO/$AO) < $maf){
                $GT = "1/1";
                $filter = "PASS" if ($filter eq ".");
                my @genomic = ($GT,$DP,$RO,$QR,$AO,$QA,$GL);
                $genomic = join(":", @genomic);
                $info = $info.";HET2A";
                apply_filter(\*OUT,$chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,$genomic);
            }else{
                $filter = "PASS" if ($filter eq ".");
                apply_filter(\*OUT,$chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,$genomic);
            }
        }
    }else{
        unless ($filter eq "."){
            print OUT "$line\n";
        }else{
            $filter = "PASS";
            apply_filter(\*OUT,$chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,$genomic);
        }
    }
}
close FH;
close OUT;

sub apply_filter{
    # constructs the output string from passed variables and prints to file
    my ($fh,$chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,$genomic)=@_;
    my @line = ($chr,$pos,$id,$ref,$alt,$qual,$filter,$info,$format,$genomic);
    print $fh join("\t",@line);
    print $fh "\n";
}

sub parse_header{
    my ($fh,$line)=@_;
    #sanity checks
    
}

sub print_usage{
    # print usage message and exit
    print STDERR "\nExample usage:\n\n";
    print STDERR "$0 [Options] --vcf <filename> or $0 --help\n";
    print STDERR "Options:\n";
    print STDERR "      --freq <proportion> = the minor allele frequency to consider a het\n\n";
    print STDERR "Required:\n";
    print STDERR "      --vcf <filename> = freebayes vcf file\n";
    exit;       
}
